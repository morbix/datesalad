//
//  User.h
//  DateSalad
//
//  Created by HP Developer on 19/11/13.
//  Copyright (c) 2013 4tap. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *email;
@property (nonatomic, retain) NSString *memberId;
@property (nonatomic, retain) NSString *facebookId;

+ (id)shared;
@end
