//
//  AppDelegate.h
//  DateSalad
//
//  Created by Felipe Malta on 18/11/13.
//  Copyright (c) 2013 4tap. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
