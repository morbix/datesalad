//
//  DSSignUpViewController.h
//  DateSalad
//
//  Created by Felipe Malta on 18/11/13.
//  Copyright (c) 2013 4tap. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DSSignUpViewController : UIViewController{
__weak IBOutlet UITextField *txtUserName;
__weak IBOutlet UITextField *txtEmailAdress;
__weak IBOutlet UITextField *txtPassword;
__weak IBOutlet UITextField *txtCpassword;

}

- (IBAction)actBtnSignIn:(id)sender;
- (IBAction)actBtnSignInFace:(id)sender;
- (IBAction)actBtnSignInTroubles:(id)sender;
@end
