//
//  DSTesteViewController.m
//  DateSalad
//
//  Created by HP Developer on 19/11/13.
//  Copyright (c) 2013 4tap. All rights reserved.
//

#import "DSTesteViewController.h"

@interface DSTesteViewController ()

@end

@implementation DSTesteViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    if ([[User shared] facebookId]) {
        if (![[[User shared] facebookId] isEqualToString:@""]) {
            self.imageProfile.profileID = [[User shared] facebookId];
        }
    }
    
    [self.labelUsername setText:[[User shared] name]];
    [self.labelEmail setText:[[User shared] email]];
    [self.labelMemberID setText:[[User shared] memberId]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnBackTouched:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{

    }];
}
@end
