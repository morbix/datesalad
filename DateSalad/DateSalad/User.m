//
//  User.m
//  DateSalad
//
//  Created by HP Developer on 19/11/13.
//  Copyright (c) 2013 4tap. All rights reserved.
//

#import "User.h"

@implementation User

+ (id)shared
{
    static User *manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [User new];
    });
    
    return manager;
}

-(NSString *)description
{
    return [NSString stringWithFormat:@"%@:%@:%@",self.name, self.email, self.memberId];
}

@end
