//
//  DSsplashViewController.h
//  DateSalad
//
//  Created by Felipe Malta on 18/11/13.
//  Copyright (c) 2013 4tap. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+WebCache.h"

@interface DSsplashViewController : UIViewController{

    __weak IBOutlet UILabel *lblAdvertisement;

}
@property (weak, nonatomic) IBOutlet UIImageView *imgAdvertisement;



-(void)goDSLogin;

-(void)parseAdvertisement:(NSString *)jsonString;

@end
