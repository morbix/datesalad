//
//  DSsplashViewController.m
//  DateSalad
//
//  Created by Felipe Malta on 18/11/13.
//  Copyright (c) 2013 4tap. All rights reserved.
//

#import "DSsplashViewController.h"

@interface DSsplashViewController ()

@end

@implementation DSsplashViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    //Call WebService
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"http://datesalad.epssquared.com/index.php?r=ad/get"]];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    //get response
    NSHTTPURLResponse* urlResponse = nil;
    NSError *error = [[NSError alloc] init];
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&error];
    NSString *result = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSLog(@"Response Code: %d", [urlResponse statusCode]);
    if ([urlResponse statusCode] >= 200 && [urlResponse statusCode] < 300) {
        
        NSString *trimmedString = [result stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        [self parseAdvertisement:trimmedString];
        
    }else{
        
        [[[UIAlertView alloc] initWithTitle:@"Error" message:@"Can't connection!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
        
    }
    
}

//Call SignUp View Controller
-(void)goDSLogin{
    
    [self performSegueWithIdentifier:@"signupViewController" sender:self];
    
}
// Get the information for webservice json response and parse it.
-(void)parseAdvertisement:(NSString *)jsonString{
    
    NSError* error;
    
    NSData* data=[jsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    if (data !=nil) {
        NSDictionary * json = [NSJSONSerialization JSONObjectWithData:data
                                                              options:kNilOptions
                                                                error:&error];
        lblAdvertisement.text= [NSString stringWithFormat:@"Powered by %@",[json objectForKey:@"title"]];
        NSLog(@"%@",[json objectForKey:@"asset"]);
        NSURL * url = [NSURL URLWithString:[json objectForKey:@"asset"]];
        [_imgAdvertisement setImageWithURL:url placeholderImage:[UIImage imageNamed:@""]];
        
        [NSTimer scheduledTimerWithTimeInterval:4.0 target:self selector:@selector(goDSLogin) userInfo:nil repeats:NO];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
