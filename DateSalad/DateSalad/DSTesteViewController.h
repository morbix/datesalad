//
//  DSTesteViewController.h
//  DateSalad
//
//  Created by HP Developer on 19/11/13.
//  Copyright (c) 2013 4tap. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import "User.h"

@interface DSTesteViewController : UIViewController

@property (weak, nonatomic) IBOutlet FBProfilePictureView *imageProfile;
@property (weak, nonatomic) IBOutlet UILabel *labelUsername;
@property (weak, nonatomic) IBOutlet UILabel *labelEmail;
@property (weak, nonatomic) IBOutlet UILabel *labelMemberID;
- (IBAction)btnBackTouched:(id)sender;
@end
