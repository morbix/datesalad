//
//  DSSignUpViewController.m
//  DateSalad
//
//  Created by Felipe Malta on 18/11/13.
//  Copyright (c) 2013 4tap. All rights reserved.
//

#import "DSSignUpViewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import "User.h"

@interface DSSignUpViewController ()

@end

@implementation DSSignUpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)createUserWithUsername:(NSString *)username andEmail:(NSString *)email andPassword:(NSString *)password andFacebookId:(NSString *)facebookId
{
    NSString *post = [NSString stringWithFormat:@"request={'username' : '%@', 'email' : '%@', 'password' : '%@'}",username, email, password];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"http://datesalad.epssquared.com/index.php?r=member/create"]];
    [request setHTTPMethod:@"POST"];
    
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    //get response
    NSHTTPURLResponse* urlResponse = nil;
    NSError *error = [[NSError alloc] init];
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&error];
    //NSString *result = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    
    if ([urlResponse statusCode] >= 200 && [urlResponse statusCode] < 300) {
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:responseData options: NSJSONReadingMutableContainers error:&error];
        //NSLog(@"%@",jsonDict.description);
        if ([jsonDict objectForKey:@"member_id"]) {
            NSString *member_id = [jsonDict objectForKey:@"member_id"];
            [[User shared] setName:username];
            [[User shared] setEmail:email];
            [[User shared] setFacebookId:facebookId];
            [[User shared] setMemberId:member_id];
            NSLog(@"User: %@", [[User shared] description]);
            [self performSegueWithIdentifier:@"testeViewController" sender:nil];
        }
        
    }else{
        
        [[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Can't create user!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
        [FBSession.activeSession closeAndClearTokenInformation];
        
    }
}

#pragma mark - IBActions
- (IBAction)actBtnSignIn:(id)sender
{
    [self createUserWithUsername:txtUserName.text andEmail:txtEmailAdress.text andPassword:txtPassword.text andFacebookId:@""];
}

- (IBAction)actBtnSignInFace:(id)sender
{
    [self openSession];
}

- (IBAction)actBtnSignInTroubles:(id)sender
{
    
}

#pragma mark - FacebookSDK
- (void)sessionStateChanged:(FBSession *)session
                      state:(FBSessionState) state
                      error:(NSError *)error
{
    switch (state) {
        case FBSessionStateOpen: {
            NSLog(@"%s",__PRETTY_FUNCTION__);
            //NSString *accessToken = [[FBSession.activeSession accessTokenData] accessToken];
            if (FBSession.activeSession.isOpen) {
                [self populateUserDetails];
            }
        }
            break;
        case FBSessionStateClosed:
        case FBSessionStateClosedLoginFailed:
            // Once the user has logged in, we want them to
            // be looking at the root view.
            [[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Can't connect!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
            [FBSession.activeSession closeAndClearTokenInformation];
            break;
        default:
            break;
    }
}

- (void)populateUserDetails
{
    if (FBSession.activeSession.isOpen) {
        [[FBRequest requestForMe] startWithCompletionHandler:
         ^(FBRequestConnection *connection,
           NSDictionary<FBGraphUser> *user,
           NSError *error) {
             if (!error) {
                 [self createUserWithUsername:user.username andEmail:[user objectForKey:@"email"] andPassword:@"" andFacebookId:user.id];
             }
         }];
    }
}

- (void)openSession
{
    [FBSession openActiveSessionWithReadPermissions:@[@"basic_info", @"email"]
                                       allowLoginUI:YES
                                  completionHandler:
     ^(FBSession *session,
       FBSessionState state, NSError *error) {
         [self sessionStateChanged:session state:state error:error];
     }];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
